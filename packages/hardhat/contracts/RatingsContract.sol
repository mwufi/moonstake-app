pragma solidity >=0.6.0 <0.9.0;

//SPDX-License-Identifier: MIT

//import "@openzeppelin/contracts/access/Ownable.sol"; //https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/Ownable.sol

contract RatingsContract {
    event NewRating(address sender, uint8 rating, string message);
    event DescriptionChanged(string newDescription);

    address owneraddress;
    modifier onlyowner {
        require(owneraddress == msg.sender);
        _;
    }

    string private name = "";
    string private description = "";

    uint256 count = 0;
    uint256 total = 0;

    uint8 min_rating = 1;
    uint8 max_rating = 5;

    constructor(
        string memory _name,
        string memory _description,
        uint8 _min_rating,
        uint8 _max_rating
    ) {
        require(_max_rating > _min_rating, "Set your limits appropriately!!");
        owneraddress = msg.sender;

        name = _name;
        description = _description;

        min_rating = _min_rating;
        max_rating = _max_rating;
    }

    function getCount() public view returns (uint256) {
        return count;
    }

    function getTotal() public view returns (uint256) {
        return total;
    }

    function rate(uint8 rating, string memory message) public {
        require(rating >= min_rating, "Rating too low");
        require(rating <= max_rating, "Rating too high");

        count += 1;
        total += rating;

        emit NewRating(msg.sender, rating, message);
    }

    function setDescription(string memory newDescription) public onlyowner {
        description = newDescription;

        emit DescriptionChanged(newDescription);
    }
}
