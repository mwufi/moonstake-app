import { useCallback, useState } from "react";
import useOnBlock from "./OnBlock";

/*
  ~ What it does? ~

  Gets your nominator state from the polkadot.js api

  ~ how can I use? ~

  const nominatorState = useNominatorState(api, address);

  ~ Features ~
  
  - same as useBalance hook

*/

export default function useNominatorState(provider, api, address) {
  const [myNom, setNominatorState] = useState();

  const pollNominatorState = useCallback(
    async (api, address) => {
      if (api && address) {
        const _nom = await api.query.parachainStaking.nominatorState(address);
        setNominatorState(_nom.toHuman());
      }
    },
    [api, address],
  );

  useOnBlock(provider, () => {
    if (api && address) {
      pollNominatorState(api, address);
    } else {
      console.log("no block", api, address);
    }
  });

  return myNom;
}
