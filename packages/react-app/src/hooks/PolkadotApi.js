import { useCallback, useState } from "react";

import moonbeamTypes from "../helpers/moonbeam-types";
import { WsProvider, ApiPromise } from "@polkadot/api";

export default function usePolkadotApi(ws_endpoint) {
  const [api, setApi] = useState();
  const [isFetching, setIsFetching] = useState(false);

  const fetchData = useCallback(async () => {
    setIsFetching(true);
    console.log("creating wsProvider");
    const wsProvider = new WsProvider(ws_endpoint);
    const api = await ApiPromise.create({
      provider: wsProvider,
      types: moonbeamTypes,
    });
    setApi(api);
  }, [ws_endpoint]);

  if (!isFetching) fetchData();
  return api;
}
