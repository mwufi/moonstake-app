import React from "react";
import { parseEther } from "@ethersproject/units";
import { BigNumber } from "@ethersproject/bignumber";
import Balance from "./Balance";

const theme = {
  cyan: "rgb(101, 193, 183)",
  magenta: "rgb(204, 47, 114)",
  gray: "#777",
};

function DetailedBalance({ free, staked, price }) {
  let _free = free || BigNumber.from(0);
  let _staked = parseEther(parseFloat(staked).toString());

  return (
    <div
      style={{
        display: "flex",
        width: "100%",
        justifyContent: "center",
      }}
    >
      <table style={{ width: "100%" }}>
        <tbody>
          <tr
            style={{
              fontStyle: "italic",
              color: theme.cyan,
            }}
          >
            <td>Free</td>
            <td>Staked</td>

            <td>Total</td>
          </tr>
          <tr>
            <td style={{ color: theme.gray }}>
              <Balance balance={_free} price={price} dollarMode={false} />
            </td>
            <td style={{ color: theme.magenta }}>
              <Balance balance={_staked} price={price} dollarMode={false} />
            </td>
            <td>
              <Balance balance={_free.add(_staked)} price={price} dollarMode={false} />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default DetailedBalance;
