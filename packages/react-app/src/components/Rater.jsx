import { Rate } from "antd";
import React from "react";

const desc = ["😡 too slow", "okay", "usable", "good", "wonderful 🎉"];

function Rater({ value, setValue }) {
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Rate tooltips={desc} onChange={setValue} value={value} />
      {value ? <h4 className="ant-rate-text">{desc[value - 1]}</h4> : ""}
    </div>
  );
}

export default Rater;
