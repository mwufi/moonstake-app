import { PageHeader } from "antd";
import React from "react";

// displays a page header

export default function Header({ children }) {
  return (
    <div className="header">
      <a
        href="https://moonbeam.network/announcements/testnet-upgrade-moonbase-alpha-v8/"
        target="_blank"
        rel="noopener noreferrer"
      >
        <PageHeader title="🏗 MoonStake" subTitle="Stake your Polkadot, with Moonbeam!" style={{ cursor: "pointer" }} />
      </a>
      {children}
    </div>
  );
}
