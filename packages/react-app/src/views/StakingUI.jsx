/* eslint-disable jsx-a11y/accessible-emoji */

import { ExclamationCircleOutlined } from "@ant-design/icons";
import { Form, Modal, InputNumber, Select, Button, Card, notification, Divider } from "antd";
import Blockies from "react-blockies";

import React, { useState, useEffect, useMemo } from "react";
import { DetailedBalance, Rater } from "../components";
import { useNominatorState } from "../hooks";

const { Option } = Select;
const { confirm } = Modal;

async function executeTransaction(tx, errMessage) {
  console.log(tx);
  console.log(` 💚 tx ${tx.hash}`);
  try {
    await tx.wait();
    console.log(` 💚 Mined`);
  } catch {
    alert(errMessage || "oopsies! something went wrong");
  }
}

function getEncouragingMessage(address) {
  // when you select a new thing to stake
  return `${address} is a good choice.`;
}

export default function StakingUI({
  api,
  customContract,
  signer,
  nominatorState,
  address,
  yourLocalBalance,
  price,
  tx,
  writeContracts,
}) {
  const [candidates, setCandidates] = useState([]);
  const stakedBalance = nominatorState?.total || 0;

  // We also do notifications
  const openNotification = (message, description) => {
    notification.info({
      message,
      description,
      placement: "bottomRight",
    });
  };

  const contractWithSigner = useMemo(() => (customContract ? customContract.connect(signer) : customContract), [
    customContract,
    signer,
  ]);

  function getCurrentBondAmount(candidate) {
    if (!nominatorState) return 0;

    const myNominations = nominatorState.nominations;
    const x = myNominations?.find(({ owner, _ }) => owner === candidate);
    const nominationAmount = x?.amount || 0;
    return nominationAmount;
  }

  async function bondDifferentAmount(candidate, amount) {
    let tx;
    let diff = parseFloat(amount) - parseFloat(getCurrentBondAmount(candidate));
    console.log(getCurrentBondAmount(candidate), amount);

    if (parseFloat(getCurrentBondAmount(candidate)) === 0) {
      console.log(`Nominating new candidate: ${candidate}, ${amount} DEV`);
      tx = await contractWithSigner.nominate(candidate, (amount * 10 ** 18).toString());
    } else if (diff > 0) {
      console.log(`Bond more: ${candidate}, ${diff * 10 ** 18}`);
      tx = await contractWithSigner.nominator_bond_more(candidate, (parseFloat(diff) * 10 ** 18).toString());
    } else if (diff < 0) {
      console.log(`Bond less: ${candidate}, ${diff * 10 ** 18}`);
      tx = await contractWithSigner.nominator_bond_less(candidate, (parseFloat(diff) * -1 * 10 ** 18).toString());
    } else {
      console.log(`The same...`);
      return;
    }
    await executeTransaction(tx, "You must have at least 5 stake...");
  }

  async function refreshNominatorState() {
    if (!api) {
      alert("oh no, you don't have a connection to Polkadot chain!");
      return;
    }
  }

  useEffect(() => {
    async function fetchData() {
      const result = await api.query.parachainStaking.candidatePool();
      setCandidates(result.toHuman());
    }

    if (api) {
      console.log("api changed..");
      fetchData();
    }
  }, [api]);

  // ratings
  const [rating, setRating] = useState("4");

  const [nomination, setNomination] = useState("");
  const nominationChoices = candidates.map(({ owner, amount }) => (
    <Option key={owner} value={owner} style={{ display: "flex", flexDirection: "column" }}>
      <div style={{ alignItems: "center", display: "flex" }}>
        <Blockies seed={owner.toLowerCase()} size={8} scale={4} />
        <div style={{ marginLeft: "10px" }}>{owner}</div>
      </div>
      <div style={{ float: "right" }}>Combined Stake: {amount}</div>
    </Option>
  ));

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalNomination, setModalNomination] = useState("");
  const [changeAmount, setChangeAmount] = useState(0);

  const showModal = nomination => {
    setIsModalVisible(true);
    setModalNomination(nomination);

    setChangeAmount(getCurrentBondAmount(nomination));
  };

  const handleOk = () => {
    alert(`Alright, staking ${changeAmount} DEV. You'll see your balance go down when this transaction is finalized!`);
    bondDifferentAmount(modalNomination, changeAmount);
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  async function unStake(address) {
    confirm({
      title: "Are you sure delete this stake?",
      icon: <ExclamationCircleOutlined />,
      content: "You'll get a notification from Metamask after the transactions is finalized!",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk() {
        console.log("unstake");
        async function f() {
          let tx = await contractWithSigner.revoke_nomination(address);
          await executeTransaction(tx, "Unstaking failed :(");
        }
        f();
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }

  const loadingText = <h2 style={{ color: "#ddd" }}>loading...</h2>;
  const modal = (
    <Modal title="Stake some Polkadot!" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
      <Blockies seed={modalNomination.toLowerCase()} size={8} scale={4} />
      <p style={{ marginBottom: 8 }}>{getEncouragingMessage(modalNomination)}</p>
      <Form.Item label="Tokens to Stake (5 minimum)">
        <InputNumber value={changeAmount} onChange={setChangeAmount} />
      </Form.Item>
    </Modal>
  );

  const loaded = api && address;
  return (
    <div>
      {/*
        ⚙️ Here is an example UI that displays and sets the purpose in your smart contract:
      */}
      <div style={{ border: "1px solid #cccccc", padding: 16, width: 400, margin: "auto", marginTop: 64 }}>
        <h2>Polkadot Staking</h2>
        <h4>Get started with staking on Polkadot!</h4>
        <Divider />
        <h2>Your Balance</h2>
        {yourLocalBalance ? (
          <DetailedBalance free={yourLocalBalance} staked={stakedBalance} price={price} />
        ) : (
          loadingText
        )}
        <Divider />
        <h2>Candidates</h2>
        <Select
          showSearch
          className="addressSelect"
          style={{ width: "100%" }}
          placeholder="Select a person"
          optionFilterProp="children"
          onChange={setNomination}
          filterOption={(input, option) => option.value.toLowerCase().indexOf(input.toLowerCase()) >= 0}
        >
          {nominationChoices}
        </Select>
        <Button style={{ margin: 8 }} type="primary" onClick={() => showModal(nomination)} disabled={!nomination}>
          Add Stake
        </Button>
        {modal}
        <Divider />
        <h2>⛏️ Your Stakes </h2>
        <div style={{ margin: 8 }}>
          {loaded ? (
            nominatorState ? (
              nominatorState.nominations?.map(({ owner, amount }) => (
                <Card
                  hoverable
                  title={owner}
                  key={owner}
                  cover={<Blockies seed={owner.toLowerCase()} size={8} scale={4} />}
                  style={{ marginTop: 8 }}
                  extra={
                    <a href="#" onClick={refreshNominatorState}>
                      Refresh
                    </a>
                  }
                >
                  <p>Amount: {amount}</p>
                  <Button
                    type="dashed"
                    danger
                    onClick={async () => {
                      await unStake(owner);
                    }}
                  >
                    Unstake
                  </Button>
                  <Button type="primary" style={{ marginLeft: 8 }} onClick={() => showModal(owner)}>
                    Change Stake
                  </Button>
                </Card>
              ))
            ) : (
              <div>
                <div>no stakes</div>
                <a href="#" onClick={refreshNominatorState}>
                  Refresh
                </a>
              </div>
            )
          ) : (
            loadingText
          )}
        </div>

        <Divider />
        <div>How did you like this app?</div>
        <Rater value={rating} setValue={setRating} />

        <Button
          style={{ margin: 8 }}
          onClick={async () => {
            const result = tx(writeContracts.RatingsContract.rate(rating, "rated MoonStake!"), update => {
              console.log("📡 Transaction Update:", update);
              if (update && (update.status === "confirmed" || update.status === 1)) {
                const msg = `🍾 Your ${rating}-star rating is now on chain @ ${update.hash}`;
                openNotification("Congratulations", msg);
                console.log(msg);
              }
            });

            console.log("awaiting metamask/web3 confirm result...", result);
            console.log(await result);
          }}
        >
          Rate this app on the blockchain!
        </Button>
        <Divider />
        <h2>The best staking app ever</h2>
        <Divider />
      </div>
    </div>
  );
}
