/* eslint-disable jsx-a11y/accessible-emoji */

import React from "react";

export default function Hints({ yourLocalBalance, mainnetProvider, price, address }) {
  return (
    <div>
      <h1 style={{ margin: 32 }}>
        <span style={{ marginRight: 8 }}>👷</span>
        About MoonStake
      </h1>

      <div style={{ margin: 32 }}>
        A app to try out the <a href="https://docs.moonbeam.network/staking/stake/">staking features</a> recently
        introduced in Moonbeam!
      </div>

      <h1 style={{ margin: 32 }}>
        <span style={{ marginRight: 8 }}>👷</span>
        What is staking?
      </h1>

      <div style={{ margin: 12 }}>
        <a href="https://docs.moonbeam.network/staking/stake/">How to Stake (moonbeam documentation)</a>
      </div>

      <div style={{ margin: 32 }}>
        <span style={{ marginRight: 8 }}>🛰</span> It's simpler on this app - all you need to do is pick a candidate, and
        press "Add Stake"!
      </div>

      <h1 style={{ margin: 32 }}>
        <span style={{ marginRight: 8 }}>👷</span>
        Features
      </h1>

      <div style={{ margin: 32 }}>
        <span style={{ marginRight: 8 }}>🚀</span>
        Try pressing some Nintendo cheatcodes for extra fun
      </div>

      <div style={{ margin: 32 }}>
        <span style={{ marginRight: 8 }}>🎛</span>
        Once you rate the app, your rating will live forever on the blockchain! (or, until Moonbase Alpha is reset)
      </div>

      <div style={{ marginTop: 32 }}>
        <span style={{ marginRight: 8 }}>🔭</span>
        This app listens to the Polkadot state so that you don't have to! It also updates your balance and stake every
        block!
      </div>

      <h1 style={{ margin: 32 }}>
        <span style={{ marginRight: 8 }}>👷</span>
        Learn More
      </h1>

      <div style={{ marginTop: 32 }}>
        <span style={{ marginRight: 8 }}>🔭</span>
        <b>How do I make my own app on Polkadot (Moonbeam)?</b>
        <div style={{ margin: 12 }}>
          🛠 Peek this app on <a href="https://gitlab.com/mwufi/moonstake-app/-/tree/master/">Gitlab</a>! 🚀
        </div>
      </div>
      <div style={{ padding: 128 }}>
        <div>
          🛠 Built with <a href="https://github.com/austintgriffith/scaffold-eth">scaffold-eth</a> and{" "}
          <a href="https://docs.moonbeam.network/">Moonbeam tech</a>🚀
        </div>
      </div>
    </div>
  );
}
