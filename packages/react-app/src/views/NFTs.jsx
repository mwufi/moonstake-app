import React from "react";
import { Empty } from "antd";

export default function NFTView() {
  return (
    <Empty
      image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
      imageStyle={{
        height: 260,
      }}
      style={{ margin: 20 }}
      description={<span>NFTs are coming soon... </span>}
    ></Empty>
  );
}
